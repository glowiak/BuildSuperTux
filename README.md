# BuildSuperTux

Tools and patches to build SuperTux

## Building

unpack tar.bz2 file, move patches to extacted dir, go to it and type:

    for PATCH_LIST in $(ls *.patch); do patch -p1 < $PATCH_LIST; done

    # when it asks 'File to patch: ', type 'src/music_manager.cpp'. This is needed to compile the game

    ./configure --prefix=/usr --sysconfdir=/etc --with-x --program-prefix="" --build=$(uname -m)-slackware-linux

    # replace $(uname -m)-slackware-linux with your glibc name

    make

    make install

    # install command will return an error, so we need to manually copy libraries:

    cp data/* /usr/share/supertux/

    cp -r data/* /usr/share/supertux/

    # start supertux

    /usr/bin/supertux